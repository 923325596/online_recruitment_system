# 招聘系统

## 项目说明
前后端分离开发，分三类用户：中介管理员、应聘人、招聘企业，实现了招聘系统的常见功能。

## 项目结构
```
├── README.md # 本文件
├── doc # 文档
├── fast-app-server # 后端代码
├── fast-app-ui # 前端代码
└── pom.xml # maven pom
```

## 技术选型

后端：
- 核心框架：Spring Boot 2.x

- 持久层框架：mybatis

- 开发/打包：idea 2019、maven3.6.x

- 数据库：MySQL5.7.x

前端
- vue 2

- ant design vue

- 开发/打包：vscode、node 8.9+、yarn 1.11+


## 本地部署

- 下载源码，解压
- 新建数据库*recruitment*，数据库编码为UTF-8
- 执行doc/db/{name}.sql文件，初始化相应模块数据
- 修改`application-dev.yml`，更新MySQL账号和密码
- 打包：进入项目根目录，执行：`mvn clean package -Dmaven.test.skip=true`
- 打包完成后会生成文件`fast-app-server/target/fast-app-server-1.0-SNAPSHOT.jar`，执行命令：`java -jar fast-app-server/target/fast-app-server-1.0-SNAPSHOT.jar`
- 在浏览器访问：http://localhost:8080/recruitment

如果使用ide，则使用Eclipse/IDEA打开项目，运行FastApplication.java，则可启动项目