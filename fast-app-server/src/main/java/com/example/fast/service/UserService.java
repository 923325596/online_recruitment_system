package com.example.fast.service;

import com.baomidou.mybatisplus.service.IService;
import com.example.fast.domain.entity.UserEntity;
import com.example.fast.domain.enums.UserTypeEnum;
import com.example.fast.domain.vo.LoginVo;
import com.example.fast.domain.vo.UserQuery;

import java.util.List;
import java.util.Optional;

/**
 * UserService
 */
public interface UserService extends IService<UserEntity> {

    /**
     * 获取用户列表
     */
    List<UserEntity> listUser(UserTypeEnum userTypeEnum);

    /**
     * 用户登录
     */
    Optional<LoginVo> login(UserQuery userQuery);

    /**
     * 用户注册
     */
    void reg(UserQuery userQuery);

    /**
     * 更新用户信息
     */
    void updateUserInfo(UserQuery userQuery);
}
