package com.example.fast.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.example.fast.common.exception.BaseException;
import com.example.fast.common.util.DTOUtils;
import com.example.fast.common.util.JwtUtils;
import com.example.fast.common.util.Md5Utils;
import com.example.fast.common.util.UIDUtils;
import com.example.fast.dao.UserDao;
import com.example.fast.domain.entity.UserEntity;
import com.example.fast.domain.enums.UserTypeEnum;
import com.example.fast.domain.vo.LoginVo;
import com.example.fast.domain.vo.UserQuery;
import com.example.fast.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * UserService
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Autowired
    private JwtUtils jwtUtils;

    /**
     * 查询用户
     */
    @Override
    public List<UserEntity> listUser(UserTypeEnum userTypeEnum) {
        return this.selectList(new EntityWrapper<UserEntity>()
                .eq("user_type", userTypeEnum));
    }

    /**
     * 用户登录
     */
    @Override
    public Optional<LoginVo> login(UserQuery userQuery) {
        UserEntity userEntity = this.baseMapper.selectByUserName(userQuery.getUsername());
        if (userEntity == null || userEntity.getUserType() != userQuery.getUserType()) {
            throw new BaseException("用户不存在");
        }
        if (userEntity.getPassword().equals(Md5Utils.encryptMd5(userQuery.getPassword(), userEntity.getSalt()))) {
            LoginVo loginVo = DTOUtils.map(userEntity, LoginVo.class);
            String token = jwtUtils.generateToken(userEntity.getId());
            loginVo.setToken(token);

            if (userEntity.getFirstLogin()) {
                userEntity.setFirstLogin(false);
                this.baseMapper.updateById(userEntity);
            }
            return Optional.of(loginVo);
        }
        return Optional.empty();
    }

    /**
     * 用户注册
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void reg(UserQuery userQuery) {
        //判断用户是否已注册
        if (null != this.baseMapper.selectByUserName(userQuery.getUsername())) {
            throw new BaseException("该用户已注册");
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(userQuery.getUsername())
                  .setPhone(userQuery.getPhone()).setSalt(UIDUtils.generateUuid8())
                  .setPassword(Md5Utils.encryptMd5(userQuery.getPassword(), userEntity.getSalt()))
                  .setUserType(userQuery.getUserType()).setIntroduction(userQuery.getIntroduction())
                  .setCreateTime(LocalDateTime.now())
                  .setCompanyName(userQuery.getCompanyName())
                  .setCompanyDesc(userQuery.getCompanyDesc());
        this.insert(userEntity);
    }

    /**
     * 更新用户信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUserInfo(UserQuery userQuery) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userQuery.getUserId());
        userEntity.setPhone(userQuery.getPhone());
        userEntity.setMail(userQuery.getMail());
        userEntity.setIntroduction(userQuery.getIntroduction());
        userEntity.setUpdateTime(LocalDateTime.now());
        userEntity.setAge(userQuery.getAge());
        userEntity.setGender(userQuery.getGender());
        userEntity.setEducation(userQuery.getEducation());
        userEntity.setWorkExperience(userQuery.getWorkExperience());
        userEntity.setProjectExperience(userQuery.getProjectExperience());
        userEntity.setLanguage(userQuery.getLanguage());
        userEntity.setProfessionalSkill(userQuery.getProfessionalSkill());
        userEntity.setSelfEvaluation(userQuery.getSelfEvaluation());
        userEntity.setCompanyName(userQuery.getCompanyName());
        userEntity.setCompanyDesc(userQuery.getCompanyDesc());

        this.updateById(userEntity);
    }
}
