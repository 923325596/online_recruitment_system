package com.example.fast.service;

import com.baomidou.mybatisplus.service.IService;
import com.example.fast.domain.entity.JobEntity;
import com.example.fast.domain.vo.JobAppliedVo;
import com.example.fast.domain.vo.JobApplyQuery;
import com.example.fast.domain.vo.JobApplyVo;
import com.example.fast.domain.vo.JobQuery;
import com.example.fast.domain.vo.JobUpdateVo;
import com.example.fast.domain.vo.JobVo;

import java.util.List;

/**
 * UserService
 */
public interface JobService extends IService<JobEntity> {

    /**
     * 获取岗位列表
     */
    List<JobVo> listJob(JobQuery jobQuery);

    /**
     * 新增岗位
     */
    void addJob(JobEntity jobEntity);

    /**
     * 审核岗位信息
     */
    void reviewJob(Long jobId, boolean result);

    /**
     * 申请岗位
     */
    void applyJob(JobApplyVo jobApplyVo);

    /**
     * 查询候选人已申请的岗位列表
     */
    List<JobAppliedVo> listCandidateAppliedJob(JobApplyQuery jobApplyQuery);

    /**
     * 查询招聘企业岗位信息的申请记录
     */
    List<JobAppliedVo> listRecruiterJobApplied(JobApplyQuery jobApplyQuery);

    /**
     * 改卷
     */
    void reviewMyJob(JobApplyQuery jobApplyQuery);

    /**
     * 删除岗位
     */
    void delJob(Long jobId);

    /**
     * 更新岗位
     */
    void updateJob(JobUpdateVo jobUpdateVo);
}
