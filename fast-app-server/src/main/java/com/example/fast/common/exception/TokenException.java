package com.example.fast.common.exception;

import static com.example.fast.common.exception.AppExceptionEnum.TOKEN_ERROR;

/**
 * token异常
 */
public class TokenException extends BaseException {
    public TokenException(String msg) {
        super(TOKEN_ERROR, msg);
    }
}
