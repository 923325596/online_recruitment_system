package com.example.fast.common.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 系统常量
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AppConstant {

    /**
     * userId
     */
    public static final String USER_KEY = "userId";
}
