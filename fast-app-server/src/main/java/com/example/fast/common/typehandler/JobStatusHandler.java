package com.example.fast.common.typehandler;

import com.example.fast.domain.enums.JobStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * JobStatusHandler
 */
@Slf4j
public class JobStatusHandler extends BaseTypeHandler<JobStatusEnum> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, JobStatusEnum parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getIndex());
    }

    @Override
    public JobStatusEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
        try {
            return JobStatusEnum.getByIndex(rs.getInt(columnName));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }

    @Override
    public JobStatusEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        try {
            return JobStatusEnum.getByIndex(rs.getInt(columnIndex));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }

    @Override
    public JobStatusEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        try {
            return JobStatusEnum.getByIndex(cs.getInt(columnIndex));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }
}