package com.example.fast.common.typehandler;

import com.example.fast.domain.enums.JobApplyStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * JobStatusHandler
 */
@Slf4j
public class JobApplyStatusHandler extends BaseTypeHandler<JobApplyStatusEnum> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, JobApplyStatusEnum parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getIndex());
    }

    @Override
    public JobApplyStatusEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
        try {
            return JobApplyStatusEnum.getByIndex(rs.getInt(columnName));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }

    @Override
    public JobApplyStatusEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        try {
            return JobApplyStatusEnum.getByIndex(rs.getInt(columnIndex));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }

    @Override
    public JobApplyStatusEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        try {
            return JobApplyStatusEnum.getByIndex(cs.getInt(columnIndex));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }
}