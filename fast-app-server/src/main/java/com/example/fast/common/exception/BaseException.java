package com.example.fast.common.exception;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import static com.example.fast.common.exception.BaseErrorEnum.EXCEPTION;

/**
 * 自定义基础异常类
 */
@Getter
@Setter
public class BaseException extends RuntimeException {

    private String code;

    /**
     * 支持自定义错误码和提示信息的异常
     *
     * @param code 错误码
     * @param message 错误信息
     */
    public BaseException(@NotNull String code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * 支持提示信息的异常，错误码默认999999
     *
     * @param message 提示信息
     */
    public BaseException(String message) {
        super(message);
        this.code = EXCEPTION.getCode();
    }

    /**
     * 支持自定义错误码、提示信息和错误超类的异常
     *
     * @param code 错误码
     * @param message 提示信息
     * @param throwable 错误超类
     */
    public BaseException(@NotNull String code, String message, Throwable throwable) {
        super(message, throwable);
        this.code = code;
    }

    /**
     * 支持错误枚举的异常
     *
     * @param errorEnum 错误枚举
     */
    public BaseException(@NotNull BaseError errorEnum) {
        super(errorEnum.getMessage());
        this.code = errorEnum.getCode();
    }

    /**
     * 支持错误枚举和提示信息的异常
     *
     * @param errorEnum 错误枚举
     * @param message 提示信息
     */
    public BaseException(@NotNull BaseError errorEnum, String message) {
        super(message);
        this.code = errorEnum.getCode();
    }
}
