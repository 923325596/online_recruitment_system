package com.example.fast.common.util;

import com.example.fast.common.exception.BaseError;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import static com.example.fast.common.exception.BaseErrorEnum.EXCEPTION;
import static com.example.fast.common.exception.BaseErrorEnum.SUCCESS;
import static com.example.fast.common.exception.BaseErrorEnum.VERIFICATION_FAILED;

/**
 * 返回数据
 */
@Getter
@Setter
@Accessors(chain = true)
public class R<T> {

    private String code;
    private String message;
    private T data;

    public static <T> R<T> of(String code, String message) {
        return new R<T>().setCode(code).setMessage(message);
    }

    public static <T> R<T> of(String code, String message, T data) {
        return new R<T>().setCode(code).setMessage(message).setData(data);
    }

    public static <T> R<T> ok() {
        return of(SUCCESS.getCode(), SUCCESS.getMessage());
    }

    public static <T> R<T> ok(String message) {
        return of(SUCCESS.getCode(), message);
    }

    public static <T> R<T> ok(T data) {
        return of(SUCCESS.getCode(), SUCCESS.getMessage(), data);
    }

    public static <T> R<T> fail(String message) {
        return of(EXCEPTION.getCode(), message);
    }

    public static <T> R<T> fail(BaseError errorEnum) {
        return of(errorEnum.getCode(), errorEnum.getMessage());
    }

    public static <T> R<T> fail(BaseError errorEnum, String message) {
        return of(errorEnum.getCode(), message);
    }

    public static <T> R<T> validate(String message) {
        return of(VERIFICATION_FAILED.getCode(), message);
    }

    public boolean success() {
        return SUCCESS.getCode().equals(code);
    }
}
