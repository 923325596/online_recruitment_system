package com.example.fast.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AppExceptionEnum implements BaseError {

    TOKEN_ERROR("010001", "token error"),
    USERNAME_OR_PASSWORD_ERROR("010002", "username or password error");

    private String code;
    private String message;
}
