package com.example.fast.common.typehandler;

import com.example.fast.domain.enums.UserTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * UserTypeHandler
 */
@Slf4j
public class UserTypeHandler extends BaseTypeHandler<UserTypeEnum> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, UserTypeEnum parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getIndex());
    }

    @Override
    public UserTypeEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
        try {
            return UserTypeEnum.getByIndex(rs.getInt(columnName));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }

    @Override
    public UserTypeEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        try {
            return UserTypeEnum.getByIndex(rs.getInt(columnIndex));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }

    @Override
    public UserTypeEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        try {
            return UserTypeEnum.getByIndex(cs.getInt(columnIndex));
        } catch (Exception e) {
            log.error("{}", e.toString(), e);
            return null;
        }
    }
}