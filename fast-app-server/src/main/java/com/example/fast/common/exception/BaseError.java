package com.example.fast.common.exception;

/**
 * 错误码接口
 */
public interface BaseError {

    /**
     * 获取错误码
     *
     * @return 错误码
     */
    String getCode();

    /**
     * 获取错误信息
     *
     * @return 错误信息
     */
    String getMessage();
}
