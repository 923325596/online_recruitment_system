package com.example.fast.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BaseErrorEnum implements BaseError {

    // 通用校验,
    SUCCESS("000000", "success"),
    EXCEPTION("999999", "unknown exception"),

    HTTP_REQUEST_FAILED("000100", "http request failed"),
    VERIFICATION_FAILED("000200", "verification failed"),
    NO_ENUM_INDEX("000300", "enum not match"),
    NULL_EXCEPTION("000101", "请求记录不存在"),

    INSERT_FAIL("000102", "fail to insert"),
    UPDATE_FAIL("000103", "fail to update"),
    DELETE_FAIL("000104", "fail to delete"),
    DATABASE_OPERATION_EXCEPTION("000400", "database operation failed"),
    RELATION_SYSTEM_EXCEPTION("000500", "relation system call failed"),

    ;

    private String code;
    private String message;
}
