package com.example.fast.common.util;

import java.util.UUID;

/**
 * UIDUtils
 */
public final class UIDUtils {

    private static final String[] CHARS = new String[]{"a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z"};

    private UIDUtils() {
    }

    public static String generateUuid32() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String generateUuid8() {
        StringBuilder stringBuilder = new StringBuilder();
        String uuid = generateUuid32();
        for (int i = 0; i < 8; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            stringBuilder.append(CHARS[x % 0x3E]);
        }
        return stringBuilder.toString();
    }

}
