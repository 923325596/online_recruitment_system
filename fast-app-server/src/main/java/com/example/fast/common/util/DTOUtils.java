package com.example.fast.common.util;

import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO转换工具
 */
@UtilityClass
public final class DTOUtils {

    private static final ModelMapper INSTANCE = new ModelMapper();

    public static <S, T> T map(S source, Class<T> targetClass) {
        return INSTANCE.map(source, targetClass);
    }

    public static <S, T> void mapTo(S source, T dist) {
        INSTANCE.map(source, dist);
    }

    public static <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        List<T> list = new ArrayList<>();
        if (CollectionUtils.isEmpty(source)) {
            return list;
        }
        for (S aSource : source) {
            T target = INSTANCE.map(aSource, targetClass);
            list.add(target);
        }
        return list;
    }
}

