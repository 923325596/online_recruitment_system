package com.example.fast.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.example.fast.domain.entity.JobEntity;

/**
 * 数据库操作
 */
public interface JobDao extends BaseMapper<JobEntity> {
}
