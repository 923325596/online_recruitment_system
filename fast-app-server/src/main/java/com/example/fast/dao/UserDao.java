package com.example.fast.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.example.fast.domain.entity.UserEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 数据库操作
 */
public interface UserDao extends BaseMapper<UserEntity> {

    @Select("select * from t_user where username = #{username}")
    UserEntity selectByUserName(@Param("username") String username);
}
