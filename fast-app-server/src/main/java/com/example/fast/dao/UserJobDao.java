package com.example.fast.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.example.fast.domain.entity.UserJobEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 数据库操作
 */
public interface UserJobDao extends BaseMapper<UserJobEntity> {

    @Select("SELECT * FROM t_user_job WHERE user_id = #{applicantId} AND job_id=#{jobId}")
    UserJobEntity selectByJobIdAndUserId(@Param("applicantId") Long applicantId, @Param("jobId") Long jobId);

    @Select("SELECT * FROM t_user_job WHERE user_id = #{applicantId}")
    List<UserJobEntity> selectByUserId(@Param("applicantId") Long applicantId);
}
