package com.example.fast.web.controller.recruiter;

import com.example.fast.common.annotation.Login;
import com.example.fast.common.annotation.LoginUser;
import com.example.fast.common.util.R;
import com.example.fast.domain.entity.JobEntity;
import com.example.fast.domain.entity.UserEntity;
import com.example.fast.domain.vo.JobApplyQuery;
import com.example.fast.domain.vo.JobQuery;
import com.example.fast.domain.vo.JobUpdateVo;
import com.example.fast.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 招聘人员岗位接口
 */
@Login
@RestController
@RequestMapping("/v1/recruiter/job")
public class RecruiterJobController {

    @Autowired
    private JobService jobService;

    /**
     * 查询自己发布的招聘信息
     *
     * @param user
     */
    @GetMapping("/own")
    public R listMyJob(@LoginUser UserEntity user) {
        JobQuery jobQuery = new JobQuery();
        jobQuery.setCreatorId(user.getId());
        return R.ok(jobService.listJob(jobQuery));
    }

    /**
     * 新增招聘信息
     *
     * @param user
     * @param jobEntity
     */
    @PostMapping
    public R addJob(@LoginUser UserEntity user, @RequestBody JobEntity jobEntity) {
        jobEntity.setCreatorId(user.getId());
        jobService.addJob(jobEntity);
        return R.ok();
    }

    /**
     * 更新招聘信息
     *
     * @param jobId
     * @param jobUpdateVo
     */
    @PostMapping("/{jobId}")
    public R updateJob(@PathVariable Long jobId, @RequestBody JobUpdateVo jobUpdateVo) {
        jobUpdateVo.setJobId(jobId);
        jobService.updateJob(jobUpdateVo);
        return R.ok();
    }

    /**
     * 删除招聘信息
     *
     * @param jobId
     */
    @DeleteMapping("/{jobId}")
    public R delJob(@PathVariable Long jobId) {
        jobService.delJob(jobId);
        return R.ok();
    }

    /**
     * 查看申请记录
     *
     * @param user
     * @param jobApplyQuery
     */
    @GetMapping("/apply/history")
    public R applyJobResult(@LoginUser UserEntity user, JobApplyQuery jobApplyQuery) {
        jobApplyQuery.setCreatorId(user.getId());
        return R.ok(jobService.listRecruiterJobApplied(jobApplyQuery));
    }

    /**
     * 审核申请记录
     *
     * @param jobApplyQuery
     */
    @PostMapping("/review")
    public R reviewJob(@RequestBody JobApplyQuery jobApplyQuery) {
        jobService.reviewMyJob(jobApplyQuery);
        return R.ok();
    }
}
