package com.example.fast.web.controller.admin;

import com.example.fast.common.annotation.Login;
import com.example.fast.common.util.R;
import com.example.fast.domain.vo.JobQuery;
import com.example.fast.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 管理员岗位管理
 */
@Login
@RestController
@RequestMapping("/v1/admin/job")
public class AdminJobController {

    @Autowired
    private JobService jobService;

    /**
     * 获取所有岗位列表
     */
    @GetMapping
    public R listJob() {
        return R.ok(jobService.listJob(new JobQuery()));
    }

    /**
     * 审核岗位
     *
     * @param jobId
     * @param result
     */
    @PostMapping("/{jobId}/review")
    public R reviewJob(@PathVariable Long jobId, boolean result) {
        jobService.reviewJob(jobId, result);
        return R.ok();
    }

}