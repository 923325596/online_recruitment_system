package com.example.fast.web.controller.candidate;

import com.example.fast.common.annotation.Login;
import com.example.fast.common.annotation.LoginUser;
import com.example.fast.common.util.R;
import com.example.fast.common.util.WordUtils;
import com.example.fast.domain.entity.UserEntity;
import com.example.fast.domain.enums.JobStatusEnum;
import com.example.fast.domain.vo.JobApplyQuery;
import com.example.fast.domain.vo.JobApplyVo;
import com.example.fast.domain.vo.JobQuery;
import com.example.fast.domain.vo.JobVo;
import com.example.fast.service.JobService;
import org.apache.commons.text.similarity.CosineSimilarity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 应聘人岗位接口
 */
@Login
@RestController
@RequestMapping("/v1/candidate/job")
public class CandidateJobController {

    private static final CosineSimilarity cosineSimilarity = new CosineSimilarity();

    @Autowired
    private JobService jobService;

    /**
     * 查询所有岗位
     *
     * @param user
     */
    @GetMapping
    public R listJob(@LoginUser UserEntity user) {
        JobQuery jobQuery = new JobQuery()
                .setStatus(JobStatusEnum.PASS)
                .setApplicantId(user.getId());
        return R.ok(jobService.listJob(jobQuery));
    }

    /**
     * 查询推荐岗位
     *
     * @param user
     * @param factor 相似度系数
     */
    @GetMapping("/recommend")
    public R listRecommendJob(@LoginUser UserEntity user, Double factor) throws IOException {
        JobQuery jobQuery = new JobQuery().setStatus(JobStatusEnum.PASS).setApplicantId(user.getId());
        List<JobVo> jobVos = jobService.listJob(jobQuery);

        // 获取用户简历相关字符串，用于分词匹配
        final Map<CharSequence, Integer> leftVector = WordUtils.parse(
                user.getIntroduction() + "," +
                        user.getLanguage() + "," +
                        user.getEducation() + "," +
                        user.getProfessionalSkill() + "," +
                        user.getProjectExperience() + "," +
                        user.getSelfEvaluation() + "," +
                        user.getWorkExperience()
        );
        // 相似度系数，默认0.2
        Double similarityFactor = factor != null ? factor : 0;
        // 过滤相关性小的招聘信息
        List<JobVo> collect = jobVos.parallelStream().filter(jobVo -> {
            final Map<CharSequence, Integer> rightVector;
            try {
                rightVector = WordUtils.parse(jobVo.getTitle() + "," + jobVo.getDescription());
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            Double similarity = cosineSimilarity.cosineSimilarity(leftVector, rightVector);
            return similarity - similarityFactor > 1e-6;

        }).collect(Collectors.toList());

        return R.ok(collect);
    }

    /**
     * 申请岗位
     *
     * @param user
     * @param jobApplyVo
     */
    @PostMapping("/apply")
    public R applyJob(@LoginUser UserEntity user, @RequestBody JobApplyVo jobApplyVo) {
        jobApplyVo.setApplicantId(user.getId());
        jobService.applyJob(jobApplyVo);
        return R.ok();
    }

    /**
     * 申请记录
     *
     * @param user
     * @param jobApplyQuery
     */
    @GetMapping("/apply/result")
    public R applyJobResult(@LoginUser UserEntity user, JobApplyQuery jobApplyQuery) {
        jobApplyQuery.setApplicantId(user.getId());
        return R.ok(jobService.listCandidateAppliedJob(jobApplyQuery));
    }

}