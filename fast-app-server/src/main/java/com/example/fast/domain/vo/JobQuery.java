package com.example.fast.domain.vo;

import com.example.fast.domain.enums.JobStatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * JobQuery
 */
@Data
@Accessors(chain = true)
public class JobQuery {

    private Long creatorId;
    private JobStatusEnum status;
    private Long applicantId;
}