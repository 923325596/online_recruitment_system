package com.example.fast.domain.enums;

import com.example.fast.common.exception.BaseException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.example.fast.common.exception.BaseErrorEnum.NO_ENUM_INDEX;

/**
 * 招聘信息审核状态
 */
@Getter
@AllArgsConstructor
public enum JobStatusEnum {

    /**
     * 待审核
     */
    TODO(0),

    /**
     * 已通过
     */
    PASS(1),

    /**
     * 未通过
     */
    NOT_PASS(2);

    private int index;

    public int getIndex() {
        return index;
    }

    public static JobStatusEnum getByIndex(int index) {
        for (JobStatusEnum constant : JobStatusEnum.values()) {
            if (constant.getIndex() == index) {
                return constant;
            }
        }
        throw new BaseException(NO_ENUM_INDEX);
    }
}
