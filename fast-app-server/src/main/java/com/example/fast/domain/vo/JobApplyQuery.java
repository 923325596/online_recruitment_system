package com.example.fast.domain.vo;

import lombok.Data;

/**
 * JobApplyQuery
 */
@Data
public class JobApplyQuery {

    private Long jobId;
    private Long applicantId;
    private Long creatorId;
    private Boolean result;
}