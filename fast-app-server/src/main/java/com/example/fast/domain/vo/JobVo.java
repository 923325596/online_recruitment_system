package com.example.fast.domain.vo;

import com.example.fast.domain.enums.JobStatusEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class JobVo {

    private Long id;
    private String title;
    private String description;
    private String testContent;
    private JobStatusEnum status;
    private Long creatorId;
    private LocalDateTime updateTime;
    private LocalDateTime createTime;

    private String companyName;
    private String companyDesc;

    private Boolean canApply;
}
