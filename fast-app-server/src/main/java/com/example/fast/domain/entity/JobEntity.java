package com.example.fast.domain.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.example.fast.domain.enums.JobStatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 招聘信息表
 */
@Data
@Accessors(chain = true)
@TableName("t_job")
public class JobEntity {

    @TableId
    private Long id;

    /**
     * 岗位名称
     */
    private String title;

    /**
     * 岗位描述
     */
    private String description;

    /**
     * 试题内容
     */
    private String testContent;

    /**
     * 岗位状态
     */
    private JobStatusEnum status;

    /**
     * 创建人
     */
    private Long creatorId;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
