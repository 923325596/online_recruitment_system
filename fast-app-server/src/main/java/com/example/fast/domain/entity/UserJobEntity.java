package com.example.fast.domain.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.example.fast.domain.enums.JobApplyStatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 用户岗位申请表
 */
@Data
@Accessors(chain = true)
@TableName("t_user_job")
public class UserJobEntity {

    @TableId
    private Long id;

    /**
     * 应聘人ID
     */
    private Long userId;

    /**
     * 岗位ID
     */
    private Long jobId;

    /**
     * 答题结果
     */
    private String testResult;

    /**
     * 答题耗时，没用上
     */
    private Integer costTime;

    /**
     * 是否通过考核
     */
    private JobApplyStatusEnum applyStatus;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
