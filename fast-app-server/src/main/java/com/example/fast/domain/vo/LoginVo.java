package com.example.fast.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginVo {

    private String username;
    private Short gender;
    private String phone;
    private String avatar;
    private String email;
    private String introduction;
    private String token;

    private Boolean firstLogin;
}
