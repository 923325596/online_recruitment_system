package com.example.fast.domain.vo;

import lombok.Data;

/**
 * JobUpdateVo
 */
@Data
public class JobUpdateVo {

    private Long jobId;
    private String title;
    private String description;
    private String testContent;
}