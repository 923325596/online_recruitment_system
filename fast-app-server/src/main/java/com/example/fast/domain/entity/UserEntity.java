package com.example.fast.domain.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.example.fast.domain.enums.UserTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 用户表
 * <p>
 * 宽表设计
 */
@Data
@Accessors(chain = true)
@TableName("t_user")
public class UserEntity {

    @TableId
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    @JSONField(serialize = false)
    private String password;

    /**
     * 用户密码盐值
     */
    @JSONField(serialize = false)
    private String salt;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 个人简介，这个看需求，可以不要的
     */
    private String introduction;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 用户类别
     */
    private UserTypeEnum userType;

    /**
     * 用户年龄
     */
    private Integer age;

    /**
     * 性别
     */
    private Integer gender;

    // 应聘人专属信息

    /**
     * mail
     */
    private String mail;

    /**
     * 教育经历
     */
    private String education;

    /**
     * 工作经历
     */
    private String workExperience;

    /**
     * 项目经验
     */
    private String projectExperience;

    /**
     * 语言
     */
    private String language;

    /**
     * 专业技能
     */
    private String professionalSkill;

    /**
     * 自我评价
     */
    private String selfEvaluation;

    /**
     * 是否第一次登录
     */
    private Boolean firstLogin;

    // 招聘公司专属信息

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 公司描述
     */
    private String companyDesc;
}
