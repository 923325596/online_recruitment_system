package com.example.fast.domain.vo;

import com.example.fast.domain.enums.UserTypeEnum;
import lombok.Data;

@Data
public class UserQuery {

    private String username;
    private String password;
    private String phone;
    private Long userId;
    private UserTypeEnum userType;
    private String introduction = "描述一下自己吧~";
    private Integer age;
    private Integer gender;

    private String mail;
    private String education;
    private String workExperience;
    private String projectExperience;
    private String language;
    private String professionalSkill;
    private String selfEvaluation;
    private Boolean firstLogin;

    private String companyName;
    private String companyDesc;
}
