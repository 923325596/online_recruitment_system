package com.example.fast.domain.vo;

import com.example.fast.domain.entity.UserEntity;
import com.example.fast.domain.enums.JobApplyStatusEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class JobAppliedVo {

    private Long id;
    private Long userId;
    private Long jobId;
    private String testResult;
    private Integer costTime;
    private JobApplyStatusEnum applyStatus;
    private LocalDateTime updateTime;
    private LocalDateTime createTime;
    private String title;
    private String description;
    private String testContent;

    private UserEntity recruiter;
    private UserEntity candidate;
}
