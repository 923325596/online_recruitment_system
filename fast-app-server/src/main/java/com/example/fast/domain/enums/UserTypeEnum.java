package com.example.fast.domain.enums;

import com.example.fast.common.exception.BaseException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.example.fast.common.exception.BaseErrorEnum.NO_ENUM_INDEX;

/**
 * 用户类型
 */
@Getter
@AllArgsConstructor
public enum UserTypeEnum {

    /**
     * 应聘者
     */
    APPLICANT(1),

    /**
     * 招聘者
     */
    RECRUITER(2),

    /**
     * 管理员
     */
    ADMIN(3);

    private int index;

    public int getIndex() {
        return index;
    }

    public static UserTypeEnum getByIndex(int index) {
        for (UserTypeEnum constant : UserTypeEnum.values()) {
            if (constant.getIndex() == index) {
                return constant;
            }
        }
        throw new BaseException(NO_ENUM_INDEX);
    }
}
