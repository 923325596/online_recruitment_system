package com.example.fast.domain.enums;

import com.example.fast.common.exception.BaseException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.example.fast.common.exception.BaseErrorEnum.NO_ENUM_INDEX;

/**
 * 岗位申请状态
 */
@Getter
@AllArgsConstructor
public enum JobApplyStatusEnum {

    /**
     * 待评改
     */
    TODO(0),

    /**
     * 已通过
     */
    PASS(1),

    /**
     * 未通过
     */
    NOT_PASS(2);

    private int index;

    public int getIndex() {
        return index;
    }

    public static JobApplyStatusEnum getByIndex(int index) {
        for (JobApplyStatusEnum constant : JobApplyStatusEnum.values()) {
            if (constant.getIndex() == index) {
                return constant;
            }
        }
        throw new BaseException(NO_ENUM_INDEX);
    }
}
