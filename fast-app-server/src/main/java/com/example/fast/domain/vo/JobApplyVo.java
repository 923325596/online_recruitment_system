package com.example.fast.domain.vo;

import lombok.Data;

/**
 * ApplyJobVo
 */
@Data
public class JobApplyVo {

    private Long jobId;
    private String testResult;
    private Long applicantId;
}