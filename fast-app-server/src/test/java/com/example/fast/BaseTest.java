package com.example.fast;

import com.alibaba.fastjson.JSON;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class BaseTest {
    @Autowired
    private WebApplicationContext context;

    protected Throwable e = new RuntimeException("查询空");

    protected MockMvc mockMvc;

    protected void print(Object o) {
        if (o == null) {
            System.err.println("printJson : result is null");
        } else {
            System.err.println("===========" + o.getClass().getName() + " result ============");
            System.err.println(o);
            System.err.println();
        }
    }

    protected void printJson(Object o) {
        if (o == null) {
            System.err.println("printJson : result is null");
        } else {
            System.err.println("===========" + o.getClass().getName() + " result ============");
            System.err.println(JSON.toJSONString(o, true));
            System.err.println();
        }
    }

    @Before
    public void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }
}
