# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.28)
# Database: recruitment
# Generation Time: 2019-12-19 12:23:10 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table t_job
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_job`;

CREATE TABLE `t_job` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `test_content` text CHARACTER SET utf8mb4 NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `creator_id` bigint(11) unsigned NOT NULL,
  `update_time` datetime NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL DEFAULT '',
  `nickname` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `salt` varchar(32) DEFAULT NULL,
  `phone` varchar(32) NOT NULL DEFAULT '',
  `introduction` varchar(1024) NOT NULL DEFAULT '',
  `user_type` tinyint(1) unsigned NOT NULL,
  `age` int(10) unsigned DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `mail` varchar(256) DEFAULT '',
  `education` varchar(2048) DEFAULT '',
  `work_experience` varchar(2048) DEFAULT '',
  `project_experience` varchar(2048) DEFAULT '',
  `language` varchar(512) DEFAULT '',
  `professional_skill` varchar(1024) DEFAULT '',
  `self_evaluation` varchar(1024) DEFAULT '',
  `company_name` varchar(128) DEFAULT NULL,
  `company_desc` varchar(2048) DEFAULT NULL,
  `first_login` tinyint(1) DEFAULT '1',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_user` (`id`, `username`, `nickname`, `password`, `salt`, `phone`, `introduction`, `user_type`, `age`, `gender`, `mail`, `education`, `work_experience`, `project_experience`, `language`, `professional_skill`, `self_evaluation`, `company_name`, `company_desc`, `first_login`, `update_time`, `create_time`)
VALUES
	(1, 'admin', '', 'afa72f5d2e13bf7a7d12a7c57e4d5f6d', 'u3bO80ZX', '1234567890', '描述一下自己吧~', 3, 0, 0, '', '', '', '', '', '', '', '', '', 0, '2019-12-16 23:53:15', '2019-12-10 21:20:58');


# Dump of table t_user_job
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_user_job`;

CREATE TABLE `t_user_job` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(18) unsigned NOT NULL,
  `job_id` bigint(18) unsigned NOT NULL,
  `cost_time` int(10) unsigned NOT NULL,
  `test_result` text CHARACTER SET utf8mb4 NOT NULL,
  `apply_status` tinyint(1) unsigned NOT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `udk_user_id_job_id` (`user_id`,`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
